import sqlite3
conn = sqlite3.connect('orgdb.sqlite')
cur = conn.cursor()
cur.execute('DROP TABLE IF EXISTS Counts')
cur.execute('CREATE TABLE Counts (org TEXT, count INTEGER)')
fn=open('mbox.txt')
for line in fn:
    line = line.rstrip()
    if len(line) < 1:
        continue
    words = line.split()
    if words[0] != "From":
        continue
    else:
        email = words[1]
        p = email.split('@')
        org = p[1]
        cur.execute('SELECT count FROM Counts WHERE org=?',(org,))
        row= cur.fetchone()
        if row is None:
            cur.execute('INSERT INTO Counts(org,count) VALUES(?,1)',(org,))
        else:
            cur.execute('UPDATE Counts SET count=count+1 where org=?',(org,))
            conn.commit()

sqlsrt='SELECT org,count FROM Counts ORDER BY count DESC LIMIT 10'
for row in cur.execute(sqlsrt):
    print(str(row[0]),row[1])
cur.close()
